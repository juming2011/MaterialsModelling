{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Free Energy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This set of commands will import the various libraries will be using.\n",
    "# You'll need to be sure to evaluate this cell or the rest of the\n",
    "# code in this notebook won't work.\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.optimize\n",
    "import scipy.integrate\n",
    "import scipy.interpolate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting the Free Energy from DFT\n",
    "\n",
    "Again we want to calculate the free energy as the DFT Total Energy plus the Vibrational Energy:\n",
    "\n",
    "$$\n",
    "F=[E_{\\mathrm{DFT}} + E_{\\mathrm{vib}}] - TS\n",
    "$$\n",
    "\n",
    "We have already calculated the DFT total energy at several volumes as follows, now we'll look at including the vibrational energy.\n",
    "\n",
    "The following cell recreates our fit of the DFT total energies from the previous notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We'll import the data we calculated before for our DFT total energy.\n",
    "dft_EvV = np.loadtxt(\"../02_Si_V/etot_v_vol.dat\")\n",
    "\n",
    "# Again we'll define the Birch Murnaghan equation of state function.\n",
    "def BirchMurnaghanE(V, E0, V0, B0, BP):\n",
    "    '''Birch-Murnaghan Equation of State for E(V).'''\n",
    "    E = E0 + 9*V0*B0/16 * (BP*((V0/V)**(2/3) - 1)**3 +\n",
    "                           ((V0/V)**(2/3) - 1)**2 * (6-4*(V0/V)**(2/3)))\n",
    "    return E\n",
    "\n",
    "# And we'll save the fit results here.\n",
    "E0_guess = dft_EvV[:, 1].min()\n",
    "V0_guess = dft_EvV[:, 0][np.argmin(dft_EvV[:, 1])]\n",
    "\n",
    "BMparams, BMcovariance = scipy.optimize.curve_fit(BirchMurnaghanE,\n",
    "                                                  dft_EvV[:, 0],\n",
    "                                                  dft_EvV[:, 1],\n",
    "                                                  p0=[E0_guess, V0_guess, 1, 1])\n",
    "V0_DFT = BMparams[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll load in the density of states data that we calculated at different volumes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read in all the DOS we calculated and save them in a python list.\n",
    "dos_list = [np.loadtxt(\"Si00-q20.dos\"),\n",
    "            np.loadtxt(\"Si01-q20.dos\"),\n",
    "            np.loadtxt(\"Si02-q20.dos\"),\n",
    "            np.loadtxt(\"Si03-q20.dos\"),\n",
    "            np.loadtxt(\"Si04-q20.dos\"),\n",
    "            np.loadtxt(\"Si05-q20.dos\")]\n",
    "# We'll also need the volumes corresponding to each DOS.\n",
    "# These can be copied from the scf output files.\n",
    "vol_list = np.array([251.1680, 256.8978, 262.7141, 268.6176, 274.6089, 280.6885])\n",
    "# We converted these to a numpy array so we can perform these useful operations on them.\n",
    "min_v = vol_list.min()\n",
    "max_v = vol_list.max()\n",
    "\n",
    "# These give the frequencies in cm-1 on the x-axis.\n",
    "# Let's convert these to Ry so they match the DFT energies.\n",
    "eV_to_cm = 8065.54429 # This is 1/hbar in the right units.\n",
    "Ry_in_eV = 13.605693\n",
    "cm_to_Ry = 1 / eV_to_cm / Ry_in_eV\n",
    "for i, dos in enumerate(dos_list):\n",
    "    # This will scale the x-axis for every dos in the list.\n",
    "    dos[:, 0] *= cm_to_Ry\n",
    "    # If we rescale the x-axis, we need to rescale the y-axis\n",
    "    # so we still integrate to the same number of total states,\n",
    "    # as these should be in states per x-axis unit.\n",
    "    dos[:, 1] /= cm_to_Ry\n",
    "\n",
    "    plt.plot(dos[:, 0], dos[:, 1], label=\"%4.3f Bohr^3\" % vol_list[i])\n",
    "\n",
    "plt.xlabel(\"Energy (Ry)\")\n",
    "plt.ylabel(\"Normal Mode DOS\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see there's clear change in the density of states with volume. Most noticebly many high frequencies seem to be decreasing in energy with increasing volume, while some of the lower ones are increasing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## T=0\n",
    "\n",
    "Let's start by finding the energy of each of these at T=0.\n",
    "\n",
    "The states will be occupied with a Bose-Einstein distribution for phonons (chemical potential is zero, so we have a Planck Distribution):\n",
    "\n",
    "$$\n",
    "n(\\epsilon) = \\frac{1}{\\mathrm{e}^{\\epsilon/kT}-1}\n",
    "$$\n",
    "\n",
    "where $n(\\epsilon)$ is the population of a mode at vibrational energy $\\epsilon$, $k$ is the Boltzmann constant, and $T$ is the absolute temperature.\n",
    "\n",
    "The energy associated with vibration will be then given by\n",
    "\n",
    "$$\n",
    "E_\\mathrm{vib}=\\int_0^\\infty [n(\\epsilon)+1/2]\\ g(\\epsilon)\\ \\epsilon\\ \\mathrm d\\epsilon\n",
    "$$\n",
    "\n",
    "where $g(\\epsilon)$ is the normal mode density of states.\n",
    "\n",
    "\n",
    "\n",
    "At T=0, the populations $n(\\epsilon)$ all become 0, and the expression for the free then energy as a function of volume becomes:\n",
    "\n",
    "$$\n",
    "F(V)=E_{\\mathrm{DFT}}(V) +\\frac{1}{2}\\int_0^\\infty g(\\epsilon, V)\\ \\epsilon\\ \\mathrm d\\epsilon\n",
    "$$\n",
    "\n",
    "As we saw from our calculations, the density of states changes as a function of volume, so we have written $g(\\epsilon, V)$ above.\n",
    "\n",
    "This additional contribution, means that the free energy is not necessarily minimized at the same volume as the DFT total energy, even at zero temperature. The latter is what would be found from a standard DFT relaxation calculation.\n",
    "\n",
    "We already saw we have an excellent fit to $E_{\\mathrm{DFT}}(V)$, so let's focus on the vibrational term. Here we use [scipy.integrate.trapz](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.trapz.html) to perform the integration numerically."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vib_E = np.array([0.5*scipy.integrate.trapz(dos[:, 0]*dos[:, 1], dos[:, 0]) for dos in dos_list])\n",
    "\n",
    "plt.plot(vol_list, vib_E, \"bo\")\n",
    "plt.xlabel(\"Volume (Bohr^3)\")\n",
    "plt.ylabel(\"Vibrational Energy (Ry)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This looks like a straight line fit should give us a good picture of how the vibrational contribution varies with volume."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def linear(x, a, b):\n",
    "    '''A linear function y=a+bx'''\n",
    "    return a + b*x\n",
    "\n",
    "lin_params, lin_covariance = scipy.optimize.curve_fit(linear, vol_list, vib_E)\n",
    "\n",
    "xplt = np.linspace(min_v, max_v, 100)\n",
    "yplt = linear(xplt, *lin_params)\n",
    "plt.plot(vol_list, vib_E, 'bo')\n",
    "plt.plot(xplt, yplt, 'r-')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now by combining our fit for the DFT energy vs volume with the fit for the vibrational energy vs volume, we'll have an expression for the free energy as a function of volume _for T=0_:\n",
    "\n",
    "$$\n",
    "F(V) = E_\\mathrm{DFT}(V) + E_\\mathrm{vib}(V)\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def FreeEvV_T0(volume, BMparams, linparams):\n",
    "    '''Return the free energy vs volume at T=0'''\n",
    "    \n",
    "    free_energy = BirchMurnaghanE(volume, *BMparams) + linear(volume, *linparams)\n",
    "    return free_energy\n",
    "\n",
    "# Generate a plot to see how different this is to our DFT total energy.\n",
    "xplt = np.linspace(dft_EvV[:, 0].min(), dft_EvV[:, 0].max(), 100)\n",
    "yplt = FreeEvV_T0(xplt, BMparams, lin_params)\n",
    "plt.plot(dft_EvV[:, 0], dft_EvV[:, 1], 'bo')\n",
    "plt.plot(xplt, yplt, 'r-')\n",
    "plt.xlabel(\"Volume (Bohr^3)\")\n",
    "plt.ylabel(\"Energy (Ry)\")\n",
    "plt.show()\n",
    "\n",
    "# Find upper and lower limits for the volume range to search as the range of input values:\n",
    "vol_bounds = [dft_EvV[:, 0].min(), dft_EvV[:, 0].max()]\n",
    "FreeEmin = scipy.optimize.minimize_scalar(FreeEvV_T0, bracket=vol_bounds, args=(BMparams, lin_params)).x\n",
    "print(\"V0 from DFT total energy: %6.3f Bohr^3\" % V0_DFT)\n",
    "print(\"V0 from Free energy: %6.3f Bohr^3 \" % FreeEmin)\n",
    "print(\"Correction wrt to DFT: %4.2f %%\" % ((FreeEmin-V0_DFT)/V0_DFT * 100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we see we have a small adjustment to our predicted equilibrium volume at T=0. For many systems this correction will be smaller than the DFT accuracy so can be neglected."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## T > 0\n",
    "\n",
    "Now if we want to find the the equilibrium volume as a function of temperature, we'll need to look at our vibrational contribution again and add in the populations at that temperature. Also the $-TS$ term in the free energy will no longer vanish so we'll need to add a function that gives us the entropy so that we can include this term.\n",
    "\n",
    "Let's start by rewriting our expression for the vibrational energy as (by inserting the expression for the Bose Einstein distribution and simplifying):\n",
    "\n",
    "$$\n",
    "E_\\mathrm{vib}=\\frac{1}{2} \\int_0^\\infty \\mathrm{coth}\\left(\\frac{\\epsilon}{2kT}\\right)\\ g(\\epsilon)\\ \\epsilon\\ \\mathrm d\\epsilon\n",
    "$$\n",
    "where $\\mathrm{coth}$ is the hyperbolic cotan: $\\mathrm{coth}(x)=\\frac{1}{\\mathrm{tanh}(x)}=\\frac{\\mathrm{cosh}(x)}{\\mathrm{sinh}(x)}$.\n",
    "\n",
    "Note: unfortunately, numpy doesn't have a `coth` function, so we'll use the inverse of `tanh` instead.\n",
    "\n",
    "The entropy term can be found from the following expression (see e.g. [Phys. Rev. B **80**, 024304 (2009)](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.80.024304)):\n",
    "\n",
    "$$\n",
    "S(T) = k\\int_0^\\infty\n",
    " g(\\epsilon)\\left\\{\n",
    "   \\frac{\\epsilon}{2kT} \\left[\n",
    "      \\mathrm{coth} \\left( \\frac{\\epsilon}{2kT} \\right) -1\n",
    "    \\right] -\n",
    "    \\mathrm{ln}\\left[\n",
    "      1-\\mathrm{exp} \\left(\\frac{-\\epsilon}{kT}\\right)\n",
    "     \\right]\n",
    "  \\right\\} \\mathrm d\\epsilon\n",
    "$$\n",
    "\n",
    "Let's try to write functions for both of these.\n",
    "\n",
    "> Note1: to keep things simple, and avoid explicit code to handle the limit as $T\\to 0$ where the various $1/T$ factors blow up, I'll assume $T>0$.\n",
    "\n",
    "> Note2: The other issue we'll have is when $\\epsilon=0$ at the left-most point of our DOS. The log argument will be 0, which will be an issue. $g(\\epsilon)$ goes to zero as $\\epsilon\\to 0$ and will dominate, so we can skip this point.\n",
    "\n",
    "Note3: I'll put the Boltzmann factor $k$ together with temperature rather than with entropy in all that follows, so I'll be leaving out the leading $k$ in the above expression for $S$. The product $TS$ will remain the same.\n",
    "\n",
    "Let's start with a function for the vibrational energy as a function of temperature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We'll ensure T>0 and restrict ourselves to positive energy values.\n",
    "# I'll use kT instead of T as an argument - as kT can have units that matches the energies\n",
    "def vib_energy(kT, dos):\n",
    "    '''Return the vibrational energy as a function of temperature.'''\n",
    "    \n",
    "    # This will generate an error and output a message if kT<= 0.\n",
    "    assert kT > 0, \"Error: This function will only work for T>0.\"\n",
    "    \n",
    "    # We use numpy.where to remove any zero or negative values from the DOS x-axis.\n",
    "    dos_pos = dos[np.where(dos[:, 0] > 0)]\n",
    "\n",
    "    integrand = dos_pos[:, 0] * dos_pos[:, 1] / np.tanh(0.5 * dos_pos[:, 0]/(2*kT))\n",
    "\n",
    "    E = 0.5 * scipy.integrate.trapz(integrand, dos_pos[:, 0])\n",
    "    return E"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k_eV_per_K = 8.6173303e-5\n",
    "Ry_to_eV = 13.605693 # 1 Ry in eV\n",
    "k_Ry_per_K = k_eV_per_K / Ry_to_eV # 1 K in Ry\n",
    "\n",
    "# Let's see how this looks for the various DOS we've calculated.\n",
    "xvals = np.linspace(10, 1000, 100)\n",
    "# This construction calculates the energy for a set of T values, for each dos\n",
    "# and adds them to a list.\n",
    "Evals = []\n",
    "for dos in dos_list:\n",
    "    Evals.append([vib_energy(T * k_Ry_per_K, dos) for T in xvals])\n",
    "    plt.plot(xvals, Evals[-1]) # The [-1] index refers to the last value\n",
    "\n",
    "plt.xlabel(\"Temperature (K)\")\n",
    "plt.ylabel(\"Vibrational Energy (Ry)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have a pretty similar dependence for all the volumes we calculated.\n",
    "\n",
    "Now in a similar way, we'll make a function for the entropy as a function of temperature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def entropy(kT, dos):\n",
    "    '''Return the entropy as a function of temperature.'''\n",
    "\n",
    "    assert kT > 0, \"Error: This function will only work for T>0.\"\n",
    "    dos_pos = dos[np.where(dos[:, 0] > 0)]\n",
    "\n",
    "    part1 = 0.5 * dos_pos[:, 0] / kT * (1/np.tanh(dos_pos[:, 0]/(2*kT)) - 1)\n",
    "    part2 = np.log(1 - np.exp(-dos_pos[:, 0]/kT))\n",
    "    integrand = dos_pos[:, 1] * (part1 - part2)\n",
    "\n",
    "    S = scipy.integrate.trapz(integrand,  dos_pos[:, 0])\n",
    "    return S"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This construction calculates the entropy for a set of T values.\n",
    "xvals = np.linspace(10, 1000, 100)\n",
    "Svals = []\n",
    "for dos in dos_list:\n",
    "    Svals.append([entropy(T * k_Ry_per_K, dos) for T in xvals])\n",
    "    plt.plot(xvals, Svals[-1])\n",
    "\n",
    "plt.xlabel(\"Temperature (K)\")\n",
    "plt.ylabel(\"Entropy\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we can see this is also pretty consistent across the different volumes we've tried."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have defined functions for all the parts of the free energy as a function of temperature, but we really need the free energy as a function of both volume and temperature, so that we can find the equilibrium volume at a given temperature.\n",
    "\n",
    "So let's write a function that will combine all the various parts to give us this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We'll be using the following functions we defined earlier:\n",
    "# - vib_energy\n",
    "# - entropy\n",
    "\n",
    "# Rather than assuming our data follows any particular function, we'll use\n",
    "# a cubic spline interpolation to find the value for a given volume.\n",
    "\n",
    "# We'll also assume we have calculated the DFT total energies on a different\n",
    "# set of volumes than the phonon density of states, for no other reason than\n",
    "# the latter are much more expensive.\n",
    "\n",
    "def FreeEvVT(volume, kT, dft_vols, dft_etots, phonon_vols, phonon_dos_list):\n",
    "    '''Return the free energy at a given temperature and volume.'''\n",
    "    \n",
    "    # First we'll interpolate the DFT energies.\n",
    "    # This generates an interpolating function.\n",
    "    e_dft_interp = scipy.interpolate.CubicSpline(dft_vols, dft_etots)\n",
    "    # This evaluates the interpolation at the input volume.\n",
    "    e_dft = e_dft_interp(volume)\n",
    "    \n",
    "    # Now we'll get the vibrational energy at each input volume and interpolate it.\n",
    "    e_vib_list = [vib_energy(kT, dos) for dos in phonon_dos_list]\n",
    "    e_vib_interp = scipy.interpolate.CubicSpline(phonon_vols, e_vib_list)\n",
    "    e_vib = e_vib_interp(volume)\n",
    "    \n",
    "    # And finally we need the entropy. We'll do this in the same way as the vibrational energy\n",
    "    s_list = [entropy(kT, dos) for dos in phonon_dos_list]\n",
    "    s_interp = scipy.interpolate.CubicSpline(phonon_vols, s_list)\n",
    "    s = s_interp(volume)\n",
    "    \n",
    "    return e_dft + e_vib - kT*s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can use this function to find the free energy vs volume at a particular temperature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "volumes = np.linspace(dft_EvV[:, 0].min(), dft_EvV[:, 0].max(), 100)\n",
    "Fvals100 = [FreeEvVT(v, 100 * k_Ry_per_K, dft_EvV[:, 0], dft_EvV[:, 1], vol_list, dos_list) for v in volumes]\n",
    "Fvals200 = [FreeEvVT(v, 200 * k_Ry_per_K, dft_EvV[:, 0], dft_EvV[:, 1], vol_list, dos_list) for v in volumes]\n",
    "Fvals300 = [FreeEvVT(v, 300 * k_Ry_per_K, dft_EvV[:, 0], dft_EvV[:, 1], vol_list, dos_list) for v in volumes]\n",
    "plt.plot(volumes, Fvals100, label=\"T=100K\")\n",
    "plt.plot(volumes, Fvals200, label=\"T=200K\")\n",
    "plt.plot(volumes, Fvals300, label=\"T=300K\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we find the minimum of the free energy as a function of volume this will let us calculate the thermal expansion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kTvals = np.linspace(1, 1000, 100)\n",
    "FreeEminvV = [scipy.optimize.minimize_scalar(FreeEvVT, \n",
    "                                             bracket=vol_bounds,\n",
    "                                             args=(kT * k_Ry_per_K,\n",
    "                                                   dft_EvV[:, 0],\n",
    "                                                   dft_EvV[:, 1],\n",
    "                                                   vol_list,\n",
    "                                                   dos_list)\n",
    "                                            ).x\n",
    "              for kT in kTvals]\n",
    "\n",
    "plt.plot(kTvals, FreeEminvV)\n",
    "plt.xlabel(\"Temperature (K)\")\n",
    "plt.ylabel(\"Volume (Bohr^3)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The thermal expansion coefficient could then be calculated from this as\n",
    "\n",
    "$$\n",
    "\\alpha_v = \\frac{1}{V}\\frac{\\partial dV}{dT}\n",
    "$$\n",
    "\n",
    "- From our plot, it looks like we have three regions:\n",
    "    1. At very low temperature the volume expands a little with temperature\n",
    "    2. Then up to around 150-180K the volume decreases with temperature\n",
    "    3. Then we have a fairly linear increase with temperature above 200K.\n",
    "- Try to find out if these three regions are seen experimentally in the same way as indicated by our calculation.\n",
    "- See if you can do this last step and find an estimate of the thermal expansion coefficient from the data we have calculated for the three temperature ranges.\n",
    "- Try and find a measured values to compare it to.\n",
    "\n",
    "The approach we've taken in this lab, where we have calculated anharmonic properties through a series of harmonic calculations is usually known as the **quasi-harmonic approximation**.\n",
    "\n",
    "While at T=0 the vibrational energy seemed pretty linear with respect to volume, we don't know if this is the case at finite temperature. And we also don't know how the entropy term behaves. If it is nonlinear, it may be better to have more phonon DOS calculations. We haven't done any convergence testing here either, so if we're within 50% or so we're doing quite well.\n",
    "\n",
    "- Try to produce plots of how both the vibrational energy and vibrational entropy change with respect to volume at 100K, 200K, and 300K.\n",
    "\n",
    "- For silicon in the diamond structure there's only one degree of freedom: the lattice constant. How might you calculate the thermal expansion of a material with more than one degree of freedom, such as carbon in the graphite structure.\n",
    "\n",
    "- This approach could also be used to calculate a phase diagram: if there are two competing structural phases, polymorphs or allotropes of a material at a given temperature and pressure, it will adopt the structure with the lowest free energy.\n",
    "\n",
    "- See if you can add an additonal phonon calculation for a volume on either side of the DFT equilibrium volume and perform this analysis again to see how the estimate of the thermal expansion coefficient is changed. I suggest doing this manually in a separate directory or directories and linking the resulting dos files back to this directory for inclusion in the analysis."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
